# Created by pyp2rpm-3.3.6
%global pypi_name python-libinput
%global srcname libinput

Name:           python-%{srcname}
Version:        0.1.0
Release:        1%{?dist}
Summary:        Object-oriented wrapper for libinput using ctypes

License:        MIT
URL:            https://github.com/OzymandiasTheGreat/python-libinput
Source0:        %{pypi_source}
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3dist(setuptools)

%description
--This package provides a pure python wrapper for *libinput*, a library that
handles input devices for display servers and other applications that need to
directly deal with input devices.It provides device detection, device handling,
input device event processing and abstraction.*libinput* does this by reading
character files in /dev/input/, so to use this package you need to run your
code as...

%package -n     python3-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{srcname}}

Requires:       python3dist(aenum)
Requires:       python3dist(monotonic)
Requires:       python3dist(selectors34)
%description -n python3-%{srcname}
--This package provides a pure python wrapper for *libinput*, a library that
handles input devices for display servers and other applications that need to
directly deal with input devices.It provides device detection, device handling,
input device event processing and abstraction.*libinput* does this by reading
character files in /dev/input/, so to use this package you need to run your
code as...


%prep
%autosetup -n %{pypi_name}-%{version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%build
%py3_build

%install
%py3_install

%files -n python3-%{srcname}
%doc README.rst
%{python3_sitelib}/libinput
%{python3_sitelib}/python_libinput-%{version}-py%{python3_version}.egg-info

%changelog
* Wed Jun 30 2021 Christian Schneider christian.schneider@id.ethz.ch - 0.1.0-1
- Initial package.
